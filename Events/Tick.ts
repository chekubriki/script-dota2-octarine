import { EventsSDK } from "wrapper/Imports"
import { AUTO_FARM_DATA } from "../data"
import { FarmSettings } from "../menu"
import { AutoFarmUnits } from "../Service/Controller"
import { AUTO_FARM_VALIDATE } from "../Service/Validation"

EventsSDK.on("Tick", () => {
	if (!AUTO_FARM_VALIDATE.IsInGame)
		return
	AUTO_FARM_DATA.AutoFarmer = new AutoFarmUnits(
		AUTO_FARM_DATA.particleSDK,
		FarmSettings,
	)
})
