import { AUTO_FARM_DATA, FarmTypeEnum } from "../data";
import { LocationFarmX } from "../Models/Location";
import { Base, IllusionUnit } from "../Models/Units";
import { ArrayExtensions, GameSleeper, Hero, LocalPlayer, ParticlesSDK, Unit } from "../wrapper/Imports";
import { EntityX } from "../X-Core/Imports";

export class ControllerMode {
	public GameSleeperX = new GameSleeper()
	public ListOfTargets = new Map<Unit, [Unit, boolean]>()
	constructor(public base: AutoFarmUnits) {
		this.Filter()
		this.Controller()
	}

	public get FarmType() {
		return this.base.configs.FarmType.selected_id as FarmTypeEnum
	}

	public get IllusionsOnCamp() {
		return this.base.configs.IllusionsOnOneCamp.value
	}

	public get FarmEnemyCamp() {
		return this.base.configs.FarmEnemyCamp.value
	}

	public get FarmAncients() {
		return this.base.configs.FarmAncientCamps.value
	}

	public get IllusionsOnLane() {
		return this.base.configs.IllusionsOnOneLane.value
	}

	public Filter() {
		EntityX.AllyCreeps.forEach(unit => {
			if (!unit.IsControllable)
				return

			if (!unit.IsAlive || !unit.IsVisible) {
				this.removeUnit(unit, AUTO_FARM_DATA.BaseUnits)
				return
			}

			this.PushUnit(unit)
		})
	}

	public removeUnit(unit: Hero, filter: Base[]) {
		const controlableUnit = filter.find(x => x.owner === unit)
		if (controlableUnit === undefined)
			return
		this.ListOfTargets.delete(unit)
		this.GameSleeperX.ResetKey(`FARM_UNIT_MOVE_${unit.Index}`)
		ArrayExtensions.arrayRemove(filter, controlableUnit)
	}

	public Dispose() {
		this.ListOfTargets.clear()
		this.GameSleeperX.FullReset()
	}

	private Controller() {

		AUTO_FARM_DATA.BaseUnits.forEach(owner => {
			const owner2 = owner.owner
			if (!owner2.IsValid || !owner2.IsAlive || !owner2.IsVisible)
				return

			owner.LaneFarm()
		})
	}

	private PushUnit(unit: Hero) {
		if (!unit.IsControllable || AUTO_FARM_DATA.BaseUnits.some(x => x.owner === unit) || unit === this.base.Owner)
			return
		AUTO_FARM_DATA.BaseUnits.push(new IllusionUnit(unit, this))
	}
}

export class AutoFarmUnits {
	public Owner = LocalPlayer!.Hero
	public LocationFarm = new LocationFarmX()
	public Controller: Nullable<ControllerMode>
	constructor(public Particle: ParticlesSDK, public configs: any) {
		this.Controller = new ControllerMode(this)
	}

	public Dispose() {
		this.Controller?.Dispose()
	}
}
