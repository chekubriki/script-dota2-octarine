import { LocalPlayer, MapArea, Team, Unit, Vector3 } from "../wrapper/Imports";
import { LocationX } from "../X-Core/Imports";

export class LocationFarmX  {

	public BotPath: Vector3[] = []
	public MidPath: Vector3[] = []
	public TopPath: Vector3[] = []
	public LaneCache = new Map<Unit, Vector3[]>()

	constructor() {

		//const flag = LocalPlayer!.Team === Team.Radiant
		//this.TopPath = (flag ? LocationX.MapLocation.RadiantTopRoute : LocationX.MapLocation.DireTopRoute)
		//this.MidPath = (flag ? LocationX.MapLocation.RadiantMiddleRoute : LocationX.MapLocation.DireMiddleRoute)
		//this.BotPath = (flag ? LocationX.MapLocation.RadiantBottomRoute : LocationX.MapLocation.DireBottomRoute)
	}

	public IsJungle(pos: Vector3) {
		return LocationX.MapLocation.DireBottomJungle.IsInside(pos)
			|| LocationX.MapLocation.DireTopJungle.IsInside(pos)
			|| LocationX.MapLocation.RadiantBottomJungle.IsInside(pos)
			|| LocationX.MapLocation.RadiantTopJungle.IsInside(pos)
	}

	public GetLane(hero: Unit): MapArea {
		const area = DotaMap.GetMapArea(hero.Position)
		switch (area[0]) {
			case MapArea.Top:
				return MapArea.Top
			case MapArea.Middle:
				return MapArea.Middle
			case MapArea.Bottom:
				return MapArea.Bottom
			case MapArea.TopJungle:
				return MapArea.Top
			case MapArea.BottomJungle:
				return MapArea.Bottom
			default:
				return MapArea.Middle
		}
	}

	public GetPath(hero: Unit): Vector3[] {
		const area = DotaMap.GetMapArea(hero.Position)
		switch (area[0]) {
			case MapArea.Top:
				return MapArea.Top
			case MapArea.Middle:
				return MapArea.Middle
			case MapArea.Bottom:
				return MapArea.Bottom
			case MapArea.TopJungle:
				return MapArea.Top
			case MapArea.BottomJungle:
				return MapArea.Bottom
			default:
				return MapArea.Middle
		}
	}

	public GetPathCache(hero: Unit): Vector3[] {
		if (!this.LaneCache.has(hero))
			this.LaneCache.set(hero, this.GetPath(hero))
		return this.LaneCache?.get(hero) ?? []
	}

	public GetNextPoint(hero: Unit): Nullable<Vector3> {
		return DotaMap.GetCreepCurrentTarget(hero.Position, hero.Team, this.GetLane(hero))?.Position
	}
}
